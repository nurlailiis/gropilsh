-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 22, 2016 at 12:13 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `gropilsh`
--

-- --------------------------------------------------------

--
-- Table structure for table `sampah`
--

CREATE TABLE IF NOT EXISTS `sampah` (
  `id_sampah` int(11) NOT NULL AUTO_INCREMENT,
  `kategori_sampah` varchar(15) NOT NULL,
  PRIMARY KEY (`id_sampah`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `sampah`
--

INSERT INTO `sampah` (`id_sampah`, `kategori_sampah`) VALUES
(1, 'Plastik'),
(2, 'Sampah Kering'),
(3, 'Sampah Basah');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(20) NOT NULL,
  `nama` varchar(20) NOT NULL,
  `poin` int(11) NOT NULL,
  `sampah_terkumpul` int(11) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `password`, `nama`, `poin`, `sampah_terkumpul`) VALUES
(1, '', 'Kelas A', 0, 0),
(2, '', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_get_sampah`
--

CREATE TABLE IF NOT EXISTS `user_get_sampah` (
  `no_history` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `kategori_sampah` varchar(15) NOT NULL,
  `jml_sampah` int(11) NOT NULL,
  `poin` int(11) NOT NULL,
  PRIMARY KEY (`no_history`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
